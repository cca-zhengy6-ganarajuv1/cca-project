const express = require('express')
const app = express()
const port = process.env.PORT || 8080;

app.use(express.urlencoded({ extended: false }));

const cors = require('cors');
app.use(cors());

app.listen(port);

app.get('/', (req, res) => {
    res.send('Astrological sign using the date by Leon Zheng. Usage: host/zodiac?date=yyyy-mm-dd');
});

app.get('/zodiac', (req, res) => {
    let dateOfBirth = req.query.date;

    dateOfBirth = dateOfBirth.split('-');
    let [year, month, day] = dateOfBirth;

    let zodiac = 'None';

    if((month == 1 && day <= 20) || (month == 12 && day >=22)) {
        zodiac = 'your capricorn';
    } else if ((month == 1 && day >= 21) || (month == 2 && day <= 18)) {
        zodiac = 'aquarius';
    } else if((month == 2 && day >= 19) || (month == 3 && day <= 20)) {
        zodiac = 'pisces';
    } else if((month == 3 && day >= 21) || (month == 4 && day <= 20)) {
        zodiac = 'aries';
    } else if((month == 4 && day >= 21) || (month == 5 && day <= 20)) {
        zodiac = 'taurus';
    } else if((month == 5 && day >= 21) || (month == 6 && day <= 20)) {
        zodiac = 'gemini';
    } else if((month == 6 && day >= 22) || (month == 7 && day <= 22)) {
        zodiac = 'cancer';
    } else if((month == 7 && day >= 23) || (month == 8 && day <= 23)) {
        zodiac = 'leo';
    } else if((month == 8 && day >= 24) || (month == 9 && day <= 23)) {
        zodiac = 'virgo';
    } else if((month == 9 && day >= 24) || (month == 10 && day <= 23)) {
        zodiac = 'libra';
    } else if((month == 10 && day >= 24) || (month == 11 && day <= 22)) {
        zodiac = 'scorpio';
    } else if((month == 11 && day >= 23) || (month == 12 && day <= 21)) {
        zodiac = 'sagittarius';
    }

    res.send(zodiac);

});